# Copyright 2019 Kristopher L. Kuhlman <klkuhlm _at_ sandia _dot_ gov>

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import mpmath as mp
import numpy as np
import invlap

fp = lambda p: 1/(p+1)**2
mft = lambda t: t*mp.exp(-t)
nft = lambda t: t*np.exp(-t)

if 1:
    print '\nSTEHFEST\n'
    degree = 16
    MP = mp.calculus.inverselaplace.Stehfest(mp.mp)
    NP = invlap.Stehfest()

    for t in [0.001,0.1,1.0,10.0]:
        MP.calc_laplace_parameter(t,degree=degree)
        NP.calc_laplace_parameter(t,degree=degree)

        print 'Stehfest: degree=%i, t=%.1g' % (degree,t)
        print 'p:'
        for j in range(degree):
            print j,MP.p[j]-NP.p[j],MP.p[j],NP.p[j]

        print 'mp.fp - np.fp, t=%.1g  ' % (t,),
        print (MP.calc_time_domain_solution(map(fp,MP.p),t) -
               NP.calc_time_domain_solution(map(fp,NP.p),t))        

    print 'V:'
    for j in range(degree):
        print j,MP.V[j]-NP.V[j],MP.V[j],NP.V[j]

    
if 1:
    print '\nFIXED TALBOT\n'
    degree = 14
    MP = mp.calculus.inverselaplace.FixedTalbot(mp.mp)
    NP = invlap.FixedTalbot()

    for t in [0.001,0.1,1.0,10.0]:
        MP.calc_laplace_parameter(t,degree=degree)
        NP.calc_laplace_parameter(t,degree=degree)

        print 'Talbot: degree=%i, t=%.1g' % (degree,t)
        print 'p:'
        for j in range(degree):
            print j,MP.p[j]-NP.p[j],MP.p[j],NP.p[j]

        print 'mp.fp - np.fp, t=%.1g  ' % (t,),
        print (MP.calc_time_domain_solution(map(fp,MP.p),t) -
               NP.calc_time_domain_solution(map(fp,NP.p),t))
            
    print 'cot_theta:'
    for j in range(0,degree-1):
        print j,MP.cot_theta[j]-NP.cot_theta[j],MP.cot_theta[j],NP.cot_theta[j]

    print 'delta:'
    for j in range(0,degree-1):
        print j,MP.delta[j]-NP.delta[j],MP.delta[j],NP.delta[j]
            
if 1:
    print '\nDE HOOG, KNIGHT & STOKES\n'
    degree = 17
    MP = mp.calculus.inverselaplace.deHoog(mp.mp)
    NP = invlap.deHoog()

    for t in [0.001,0.1,1.0,10.0]:
        NP.calc_laplace_parameter(t,degree=degree)
        MP.calc_laplace_parameter(t,degree=degree,alpha=NP.alpha,tol=NP.tol)
        
        print 'de Hoog: degree=%i, t=%.1g' % (degree,t)
        print 'np: alpha=%.2g, tol=%.2g, gamma=%.2g' % (NP.alpha,NP.tol,NP.gamma),    
        print '   mp: alpha=%.2g, tol=%.2g, gamma=%.2g' % (MP.alpha,MP.tol,MP.gamma)    
        print 'p:'
        for j in range(degree):
            print j,MP.p[j]-NP.p[j],MP.p[j],NP.p[j]

        print 'mp.fp - np.fp, t=%.1g  ' % (t,),
        print (MP.calc_time_domain_solution(map(fp,MP.p),t) -
               NP.calc_time_domain_solution(map(fp,NP.p),t))
        
