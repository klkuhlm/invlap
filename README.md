This is a numpy/scipy version of the numerical inverse Laplace transform algorithms committed to mpmath.

http://mpmath.org/doc/current/calculus/inverselaplace.html

The "fixed Talbot", Stehfest, and "de Hoog, Knight & Stokes" algorithms are implemented.

These work with float64 and complex128 data types available in numpy, rather than arbitrary precision floating point numbers. This makes them simpler to use, but somewhat less effective and accurate.